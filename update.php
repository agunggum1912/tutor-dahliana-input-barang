<!DOCTYPE html>
<html>
<head>
	<title>Tutor Input Barang</title>
	<link rel="stylesheet" type="text/css" href="style2.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--[if lt IE 9]> <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> <![endif]-->
	<?php
		include 'koneksi.php';

		if (isset($_POST['hapus'])) {
			$id = $_POST['id'];
			$qryhapus = "DELETE FROM tb_barang WHERE id=$id";
			$hasilhapus = mysqli_query($koneksi, $qryhapus) or die ("Query Hapus Salah!");
			echo "<script>alert('Data telah terhapus.') ;window.location='index.php'; </script>";

		}

		if (isset($_POST['update'])) {
			$id = $_POST['id'];
			$nama = $_POST['nama'];
			$kategori = $_POST['kategori'];
			$warna = $_POST['warna'];
			$merk = $_POST['merk'];
			$ukuran = $_POST['ukuran'];
			$stok = $_POST['stok'];

			if (empty($nama)) {
				echo "<script>alert('Name harus di isi!');history.go(-1)</script>";
			}elseif (empty($kategori)) {
				echo "<script>alert('Kategori harus di pilih!');history.go(-1)</script>";
			}elseif (empty($warna)) {
				echo "<script>alert('Warna harus di pilih!');history.go(-1)</script>";
			}elseif (empty($merk)) {
				echo "<script>alert('Merk harus di pilih!');history.go(-1)</script>";
			}elseif (empty($ukuran)) {
				echo "<script>alert('Ukuran harus di pilih!');history.go(-1)</script>";
			}elseif (empty($stok)) {
				echo "<script>alert('Stok harus di isi!');history.go(-1)</script>";
			}else{
			$qryupdate = "UPDATE tb_barang SET nama='$nama', kategori='$kategori', warna='$warna', merk='$merk', ukuran='$ukuran', stok='$stok' WHERE id='$id'";
			$hasilupdate = mysqli_query($koneksi, $qryupdate) or die ("Query Update Salah");
			echo "<script>alert('Data telah terupdate.') ;window.location='index.php'; </script>";				
			}
		}

		$id = $_GET['id'];
		$strSQL = "SELECT * FROM tb_barang WHERE id='$id'";
		$query = mysqli_query($koneksi, $strSQL) or die ("Query Get salah!");
		$row = mysqli_fetch_array($query);
		{
			$nama = $row['nama'];
			$kategori = $row['kategori'];
			$warna = $row['warna'];
			$merk = $row['merk'];
			$ukuran = $row['ukuran'];
			$stok = $row['stok'];
		}
	?>
</head>
<body>
	<div id="header">
		<h1>UPDATE BARANG</h1>
	</div>
	</br>

	<div class="hidden"></div>

	<form action="" method="post" name="save">
		<div class="tengah">
			<div class="kotak">
				<table class="tabel" border="0">
					<tr>
						<td>Nama</td>
						<td>:</td>
						<td><input class="textbox" type="text" name="nama" value="<?php echo $nama; ?>"></td>
					</tr>
					<tr>
						<td>Kategori</td>
						<td>:</td>
						<td>
							<select class="textbox2" name="kategori">
								<option value="<?php echo $kategori; ?>"><?php echo $kategori; ?></option>
								<option value="Sport">Sport</option>
								<option value="Boots">Boots</option>
								<option value="Casual">Casual</option>
								<option value="Running">Running</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Warna</td>
						<td>:</td>
						<td>
							<select class="textbox2" name="warna">
								<option value="<?php echo $warna; ?>"><?php echo $warna; ?></option>
								<option value="Biru">Biru</option>
								<option value="Hijau">Hijau</option>
								<option value="Hitam">Hitam</option>
								<option value="Merah">Merah</option>
								<option value="Pink">Pink</option>
								<option value="Putih">Putih</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Merk</td>
						<td>:</td>
						<td>
							<select class="textbox2" name="merk">
								<option value="<?php echo $merk; ?>"><?php echo $merk; ?></option>
								<option value="Adidas">Adidas</option>
								<option value="Converse">Converse</option>
								<option value="New Balance">New Balance</option>
								<option value="Nike">Nike</option>
								<option value="Vans">Vans</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Ukuran</td>
						<td>:</td>
						<td>
							<select class="textbox2" name="ukuran">
								<option value="<?php echo $ukuran; ?>"><?php echo $ukuran; ?></option>
								<option value="36">36</option>
								<option value="37">37</option>
								<option value="38">38</option>
								<option value="39">39</option>
								<option value="40">40</option>
								<option value="41">41</option>
								<option value="42">42</option>
								<option value="43">43</option>
								<option value="44">44</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Stok</td>
						<td>:</td>
						<td><input class="textbox" type="text" name="stok" value="<?php echo $stok; ?>"></td>
					</tr>
					<tr>
						<td colspan="3">
							<input type="hidden" name="id" value="<?php echo $id; ?>">
							<input class="btn btn-biru2" type="submit" name="update" value="Update">
							<input class="btn btn-merah" type="submit" name="hapus" value="Delete">
							<a class="btn btn-dark2" href="index.php">Back</a>
						</td>
					</tr>
		</table>
	</form>
</body>
</html>