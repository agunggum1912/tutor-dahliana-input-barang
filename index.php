<!DOCTYPE html>
<html>
<head>
	<title>Tutor Input Barang</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--[if lt IE 9]> <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> <![endif]-->
	<?php
		include 'koneksi.php';

		$strSQL = "SELECT * FROM tb_barang";
		$query = mysqli_query ($koneksi, $strSQL) or die ("query barang salah");
	?>
</head>
<body>
	<div id="header">
		<h1>TABEL BARANG</h1>
	</div>
	</br>

	<div class="hidden"></div>
	<a href="tambah.php" class="jarak-left1 btn btn-biru">Tambah</a>
	</br>
	</br>

	<div class="tengah">
		<div class="kotak">
			<table border="0" class="tabel2">
				<tr>
					<td>
						Nama : Dahliana Hutahaean </br>
						NIM&emsp;: 2016804179
					</td>
				</tr>
			</table>
			</br>
			<table border="0" class="tabel">
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Kategori</th>
					<th>Warna</th>
					<th>Merk</th>
					<th>Ukuran</th>
					<th>Stok</th>
					<th>Proses</th>
				</tr>
				<?php
					$no = 0;
					while ($row = mysqli_fetch_array($query)) {
						$no++;
				?>
				<tr>
					<tbody>
					<td><?php echo $no; ?></td>
					<td style="text-align: left;"><?php echo $row['nama']; ?></span></td>
					<td><?php echo $row['kategori'];?></td>
					<td><?php echo $row['warna'];?></td>
					<td><?php echo $row['merk'];?></td>
					<td><?php echo $row['ukuran'];?></td>
					<td><?php echo $row['stok'];?></td>
					<td><a href="update.php?id=<?php echo $row['id'] ;?>" class="btn btn-hijau">Update</a></td>
					</tbody>
				</tr>
				<?php };?>
			</table>
		</div>
	</div>
</body>
</html>