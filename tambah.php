<!DOCTYPE html>
<html>
<head>
	<title>Tutor Input Barang</title>
	<link rel="stylesheet" type="text/css" href="style2.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--[if lt IE 9]> <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> <![endif]-->


	<?php
		include 'koneksi.php';

		if (isset($_POST['save'])) {
			$nama = $_POST['nama'];
			$kategori = $_POST['kategori'];
			$warna = $_POST['warna'];
			$merk = $_POST['merk'];
			$ukuran = $_POST['ukuran'];
			$stok = $_POST['stok'];

			if (empty($nama)) {
				echo "<script>alert('Name harus di isi!');history.go(-1)</script>";
			}elseif (empty($kategori)) {
				echo "<script>alert('Kategori harus di pilih!');history.go(-1)</script>";
			}elseif (empty($warna)) {
				echo "<script>alert('Warna harus di pilih!');history.go(-1)</script>";
			}elseif (empty($merk)) {
				echo "<script>alert('Merk harus di pilih!');history.go(-1)</script>";
			}elseif (empty($ukuran)) {
				echo "<script>alert('Ukuran harus di pilih!');history.go(-1)</script>";
			}elseif (empty($stok)) {
				echo "<script>alert('Stok harus di isi!');history.go(-1)</script>";
			}else{
				$query = "INSERT INTO tb_barang(id,nama,kategori,warna,merk,ukuran,stok) VALUES (NULL,'$nama','$kategori','$warna','$merk','$ukuran','$stok')";
        		$hasil = mysqli_query($koneksi, $query) or die ("Query Insert Salah");
				echo "<script>alert('Telah berhasil di tambahkan.') ;window.location='index.php'; </script>";
			}
		}
	?>
</head>
<body>
	<div id="header">
		<h1>TAMBAH BARANG</h1>
	</div>
	</br>

	<div class="hidden"></div>

	<form action="" method="post" name="save">
		<div class="tengah">
			<div class="kotak">
				<table border="0" class="tabel">
					<tr>
						<td>Nama</td>
						<td>:</td>
						<td><input class="textbox" type="text" name="nama"></td>
					</tr>
					<tr>
						<td>Kategori</td>
						<td>:</td>
						<td>
							<select class="textbox2" name="kategori">
								<option value="Sport">Sport</option>
								<option value="Boots">Boots</option>
								<option value="Casual">Casual</option>
								<option value="Running">Running</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Warna</td>
						<td>:</td>
						<td>
							<select class="textbox2" name="warna">
								<option value="Biru">Biru</option>
								<option value="Hijau">Hijau</option>
								<option value="Hitam">Hitam</option>
								<option value="Merah">Merah</option>
								<option value="Pink">Pink</option>
								<option value="Putih">Putih</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Merk</td>
						<td>:</td>
						<td>
							<select  class="textbox2" name="merk">
								<option value="Adidas">Adidas</option>
								<option value="Converse">Converse</option>
								<option value="New Balance">New Balance</option>
								<option value="Nike">Nike</option>
								<option value="Vans">Vans</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Ukuran</td>
						<td>:</td>
						<td>
							<select class="textbox2" name="ukuran">
								<option value="36">36</option>
								<option value="37">37</option>
								<option value="38">38</option>
								<option value="39">39</option>
								<option value="40">40</option>
								<option value="41">41</option>
								<option value="42">42</option>
								<option value="43">43</option>
								<option value="44">44</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Stok</td>
						<td>:</td>
						<td><input class="textbox" type="text" name="stok"></td>
					</tr>
					<tr>
						<td colspan="3">
							<center>
								<input class="btn btn-biru2" type="submit" name="save" value="Save">
								<a class="btn btn-dark" href="index.php">Back</a>
							</center>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</form>
</body>
</html>